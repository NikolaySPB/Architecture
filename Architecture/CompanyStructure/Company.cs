﻿using System.Collections.Generic;
using Architecture.Staff;

namespace Architecture.CompanyStructure
{
    /// <summary>
    /// Компания
    /// </summary>
    class Company
    {    
        /// <summary>
        /// Дирекции компании
        /// </summary>
        public List<Directorate> Directorates = new List<Directorate>();         

        /// <summary>
        /// Добавить дирекцию 
        /// </summary>
        /// <param name="directorate">дирекция</param>
        public void AddDirectorate(Directorate directorate)
        {
            Directorates.Add(directorate);
        }
    }
}
