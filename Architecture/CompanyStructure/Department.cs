﻿using System.Collections.Generic;
using Architecture.Staff;

namespace Architecture.CompanyStructure
{
    /// <summary>
    /// Отдел
    /// </summary>
    class Department
    {
        public int Id { get; private set; }
        /// <summary>
        /// Название отдела
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Электронная почта отдела
        /// </summary>
        public string DepartmentEmail { get; set; }

        /// <summary>
        /// Список сотрудников
        /// </summary>
        public List<Employee> Employees = new List<Employee>();

        public Department()
        {
        }

        public Department(int id)
        {
            Id = id;
        }

        public override string ToString()
        {
            return Name; 
        }
    }
}
