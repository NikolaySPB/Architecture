﻿using Architecture.Staff;
using System.Collections.Generic;

namespace Architecture.CompanyStructure
{
    /// <summary>
    /// Дирекция
    /// </summary>
    class Directorate
    {
        /// <summary>
        /// Отделы дирекции
        /// </summary>
        public List<Department> Departments = new List<Department>();

        /// <summary>
        /// Id дирекции
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Название дирекции
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Электронная почта дирекции
        /// </summary>
        public string DirectorateEmail { get; set; }

        /// <summary>
        /// Список сотрудников
        /// </summary>
        public List<Employee> Employees = new List<Employee>();

        public Directorate()
        {
        }

        public Directorate(int id)
        {
            Id = id;
        }

        /// <summary>
        /// Добавить отдел в дирекцию 
        /// </summary>
        /// <param name="department">отдел</param>
        public void AddDepartment(Department department)
        {
            Departments.Add(department);
        }
    }
}
