﻿using Architecture.CompanyStructure;
using Architecture.Staff;
using System;
using System.Collections.Generic;

namespace Architecture.Documents
{
    class Document
    {
        public string Name { get; private set; }

        Company company;

        Employee employee;
       
        List<Employee> employees = new List<Employee>();

        public Document(Company company, Employee employee, string name)
        {
            this.company = company;
            this.employee = employee;
            Name = name;           
        }                

        /// <summary>
        /// Отправить документ на подписи
        /// </summary>       
        public void SendDocument(params Department[] departments)
        {
            GetListLeaders(departments);

            foreach (var empl in employees)
            {
                if(!empl.SignDocument(company, employee, this))
                {
                    Console.WriteLine($"Документ не подписан в дирекции: {empl.GetDepartment(company)}, руководителем: {empl}");
                    break;
                }
            }
        }

        /// <summary>
        /// Получить список руководителей для подписи
        /// </summary>        
        private void GetListLeaders(Department[] departments)
        {
            foreach (var depar in departments)
            {
                foreach (var empl in depar.Employees)
                {
                    if (empl.PositionStatus == PositionStatus.Head)
                    {
                        employees.Add(empl);
                    }
                }
            }
        }
    }
}
