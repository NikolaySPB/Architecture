﻿using Architecture.CompanyStructure;
using Architecture.Documents;
using Architecture.Staff;
using System;
using System.Collections.Generic;

namespace Architecture
{
    class Program
    {
        static void Main(string[] args)
        {
            Company company = new Company();

            var ManagementDirect = new Directorate(1)
            {
                Name = "Управление компании"
            };
            company.AddDirectorate(ManagementDirect);
            var Management = new Department(1)
            {
                Name = "Управление",
                Employees = new List<Employee>(),
                DepartmentEmail = "Management@mail.ru"
            };
            ManagementDirect.AddDepartment(Management);

            var SalesDirect = new Directorate(2)
            {
                Name = "Дирекция по продажам"
            };
            company.AddDirectorate(SalesDirect);
            var Sales = new Department(1)
            {
                Name = "Отдел продаж",
                Employees = new List<Employee>(),
                DepartmentEmail = "Sales@mail.ru"
            };
            var AdvertisementsAndPR = new Department(2)
            {
                Name = "Отдел рекламы и PR",
                Employees = new List<Employee>(),
                DepartmentEmail = "AdvertisementsAndPR@mail.ru"
            };
            SalesDirect.AddDepartment(Sales);
            SalesDirect.AddDepartment(AdvertisementsAndPR);

            var ProductionDirect = new Directorate(3)
            {
                Name = "Производство"
            };
            company.AddDirectorate(ProductionDirect);
            var Production = new Department(1)
            {
                Name = "Производство",
                Employees = new List<Employee>(),
                DepartmentEmail = "Production@mail.ru"
            };
            ProductionDirect.AddDepartment(Production);

            var TechnicalDirect = new Directorate(4)
            {
                Name = "Техническая дирекция"
            };
            company.AddDirectorate(TechnicalDirect);
            var EquipmentService = new Department(1)
            {
                Name = "Отдел ремонта и технического обслуживания оборудования",
                Employees = new List<Employee>(),
                DepartmentEmail = "EquipmentService@mail.ru"
            };
            var SecurityWork = new Department(2)
            {
                Name = "Отдел охраны труда и технического контроля",
                Employees = new List<Employee>(),
                DepartmentEmail = "SecurityWork@mail.ru"
            };
            TechnicalDirect.AddDepartment(EquipmentService);
            TechnicalDirect.AddDepartment(SecurityWork);

            var EconomicsFinanceDirect = new Directorate(5)
            {
                Name = "Дирекция по экономике и финансам"
            };
            company.AddDirectorate(EconomicsFinanceDirect);
            var HumanResources = new Department(1)
            {
                Name = "Отдел кадров",
                Employees = new List<Employee>(),
                DepartmentEmail = "HumanResources@mail.ru"
            };
            var Bookkeeping = new Department(2)
            {
                Name = "Бухгалтерия",
                Employees = new List<Employee>(),
                DepartmentEmail = "Bookkeeping@mail.ru"
            };
            var Finance = new Department(3)
            {
                Name = "Финансовый отдел",
                Employees = new List<Employee>(),
                DepartmentEmail = "Finance@mail.ru"
            };
            var Economic = new Department(4)
            {
                Name = "Экономический отдел",
                Employees = new List<Employee>(),
                DepartmentEmail = "Economic@mail.ru"
            };
            EconomicsFinanceDirect.AddDepartment(HumanResources);
            EconomicsFinanceDirect.AddDepartment(Bookkeeping);
            EconomicsFinanceDirect.AddDepartment(Finance);
            EconomicsFinanceDirect.AddDepartment(Economic);

            var LogisticsDirect = new Directorate(6)
            {
                Name = "Управление по логистике"
            };
            company.AddDirectorate(LogisticsDirect);
            var Logistics = new Department(1)
            {
                Name = "Транспортный отдел",
                Employees = new List<Employee>(),
                DepartmentEmail = "Logistics@mail.ru"
            };
            var Procurement = new Department(2)
            {
                Name = "Отдел закупок",
                Employees = new List<Employee>(),
                DepartmentEmail = "Procurement@mail.ru"
            };
            var Stock = new Department(3)
            {
                Name = "Склад",
                Employees = new List<Employee>(),
                DepartmentEmail = "Stock@mail.ru"
            };
            LogisticsDirect.AddDepartment(Logistics);
            LogisticsDirect.AddDepartment(Procurement);
            LogisticsDirect.AddDepartment(Stock);

            var LegalDirect = new Directorate(7)
            {
                Name = "Юридическая дирекция"
            };
            company.AddDirectorate(LegalDirect);
            var Legal = new Department(1)
            {
                Name = "Юридический отдел",
                Employees = new List<Employee>(),
                DepartmentEmail = "Legal@mail.ru"
            };
            LegalDirect.AddDepartment(Legal);

            var SecretariatDirect = new Directorate(8)
            {
                Name = "Секретариат"
            };
            company.AddDirectorate(SecretariatDirect);
            var Office = new Department(1)
            {
                Name = "Канцелярия",
                Employees = new List<Employee>(),
                DepartmentEmail = "Office@mail.ru"
            };
            var Archive = new Department(2)
            {
                Name = "Архив",
                Employees = new List<Employee>(),
                DepartmentEmail = "Archive@mail.ru"
            };
            SecretariatDirect.AddDepartment(Office);
            SecretariatDirect.AddDepartment(Archive);

            var ItDirect = new Directorate(9)
            {
                Name = "ИТ дирекция"
            };
            company.AddDirectorate(ItDirect);
            var IT = new Department(1)
            {
                Name = "IT-отдел",
                Employees = new List<Employee>(),
                DepartmentEmail = "IT@mail.ru"
            };
            ItDirect.AddDepartment(IT);

            Employee IvanovII = new Employee("Иванов", "Иван", "Иванович", "Ген. директор", PositionStatus.Head, "dir@mail.ru");
            Employee SidorovSS = new Employee("Сидоров", "Сидр", "Сидорович", "Главный бухгалтер", PositionStatus.Head, "sidorov@mail.ru");
            Employee RomanovRR = new Employee("Романов", "Роман", "Романович", "Руководитель IT направления", PositionStatus.Head, "romanov@mail.ru");
            Employee PetrovPP = new Employee("Петров", "Петр", "Петрович", "Разработчик", PositionStatus.Staff, "petrov@mail.ru");

            Management.Employees.Add(IvanovII);
            Bookkeeping.Employees.Add(SidorovSS);
            IT.Employees.Add(RomanovRR);
            IT.Employees.Add(PetrovPP);
            
            Document document1 = new Document(company, PetrovPP, "Заявление на отпуск");

            document1.SendDocument(IT, Bookkeeping, Management);

            Console.ReadLine();
        }
    }
}
