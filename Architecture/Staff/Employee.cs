﻿using Architecture.CompanyStructure;
using Architecture.Documents;
using System;

namespace Architecture.Staff
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    class Employee
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// Статус должности
        /// </summary>
        public PositionStatus PositionStatus { get; private set; }

        /// <summary>
        /// Электронная почта
        /// </summary>
        public string Email { get; set; }

        public Employee(string surname, string name, string middleName, string position, PositionStatus positionStatus, string email)
        {
            Surname = surname;
            Name = name;
            MiddleName = middleName;
            Position = position;
            PositionStatus = positionStatus;
            Email = email;
        }

        public Directorate GetDirectorate(Company company)
        {
            Directorate directorate = new Directorate();
            foreach (var direct in company.Directorates)
            {
                foreach (var depart in direct.Departments)
                {
                    if (depart.Employees.Contains(this))
                        directorate = direct;
                }
            }
            return directorate;
        }

        public Department GetDepartment(Company company)
        {
            Department department = new Department();
            foreach (var direct in company.Directorates)
            {
                foreach (var depart in direct.Departments)
                {
                    if(depart.Employees.Contains(this)) 
                        department = depart;                                        
                }
            }
            return department;
        }

        public bool SignDocument(Company company, Employee employee, Document document)
        {
            Console.WriteLine($"\nПоступил документ в отдел: {GetDepartment(company)}.\nТип документа: '{document.Name}'\nОт: {employee} \nИз отдела: {employee.GetDepartment(company)}");            
            Console.Write("Подписать документ? Выберите вариант (Y - да, N - нет): ");
            var result = Console.ReadLine();
            return result == "Y" ? true : false;
        }

        public override string ToString()
        {
            return $"{Surname} {Name} {MiddleName} Должность: {Position}";
        }
    }
}
