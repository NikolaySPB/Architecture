﻿namespace Architecture.Staff
{
    /// <summary>
    /// Статус должности
    /// </summary>
    enum PositionStatus
    {
        /// <summary>
        /// Руководитель
        /// </summary>
        Head,

        /// <summary>
        /// Заместитель руководителя
        /// </summary>
        DeputyHead,

        /// <summary>
        /// Штатный сотрудник отдела
        /// </summary>
        Staff,

        /// <summary>
        /// Стажер
        /// </summary>
        Trainee
    }
}
